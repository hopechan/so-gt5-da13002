#include <unistd.h>
#include <fcntl.h>

int main(){
    char *archivo = "./test.txt";
    char buffer[4096];
    ssize_t tamanio;
    int fichero = open(archivo, O_RDONLY);

    if (fichero >= 0){
        while (tamanio = read(fichero, buffer, sizeof buffer), tamanio > 0){
            write(1, buffer, tamanio);
        }
    }
    close(fichero);
    return 0;
}
