#include <stdio.h>
#include <unistd.h>
#include <string.h>

void main(){
    /**
     * Ejercicio 1: Escribir Sistemas Operativos 2020- sin utilizar printf() 
     * Parametros de la funcion write()
     * a.- 1 representa la salida estandar
     * b.- frase es el array que contiene el string a imprimir
     * c.- strlen(frase) devuelve el tamaño del array
    */
    char frase[] = "Sistemas Operativos 2020-";
    write(1, frase, strlen(frase));
}
