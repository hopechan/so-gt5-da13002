#include <unistd.h>
#include <fcntl.h>


int copiar(char *archivo_origen, char *archivo_copia, char *buffer);

void main(){
    char *archivo_origen = "./original.txt";
    char *archivo_copia = "./original(1).txt";
    char buffer[4096];
    copiar(archivo_origen, archivo_copia, buffer);
}

int copiar(char *archivo_origen, char *archivo_copia, char *buffer){
    ssize_t nread;
    int fd_from = open(archivo_origen, O_RDONLY);
    int fd_to = open(archivo_copia, O_WRONLY | O_CREAT, 0666);

    /* If one of the file descriptors is not valid */
    if (fd_from < 0 || fd_to < 0){
        return -1;
    }

    while (nread = read(fd_from, buffer, sizeof buffer), nread > 0){
        write(fd_to, buffer, nread);
    }

    close(fd_from);
    close(fd_to);
}
